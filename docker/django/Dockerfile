ARG PYTHON_VERSION=3.9-slim-buster
ARG user_uid=1000
ARG user_gid=1000

# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION} as python

# Python build stage
# -------------------
FROM python as python-build-stage-prod

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  # psycopg2 dependencies
  libpq-dev

COPY ./requirements.txt .

# Create Python Dependency and Sub-Dependency Wheels.
RUN pip wheel --wheel-dir /usr/src/app/wheels -r requirements.txt


# Python build stage dev
# -----------------------
FROM python-build-stage-prod as python-build-stage-dev

COPY ./requirements-dev.txt .

# Create Python Dependency and Sub-Dependency Wheels.
RUN pip wheel --wheel-dir /usr/src/app/wheels -r requirements-dev.txt


# Python 'run' stage
# -------------------
FROM python as python-run-prod

ARG user_uid
ARG user_gid
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# create user
RUN set -x && \
    groupadd -g $user_gid user && \
    useradd -u $user_uid -g user -m user
# create some directories
RUN set -x && \
    mkdir /app && \
    chown user:user /app


# Install required system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
  # psycopg2 dependencies
  libpq-dev \
  # Translations dependencies
  gettext \
  # cleaning up unused files
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

# copy python dependency wheels from python-build-stage
COPY --from=python-build-stage-prod /usr/src/app/wheels  /wheels/

# use wheels to install python dependencies
RUN pip install --no-cache-dir --no-index --find-links=/wheels/ /wheels/* \
	&& rm -rf /wheels/


COPY --chown=user:user manage.py /app/
COPY --chown=user:user freshy /app/freshy
COPY --chown=user:user ./docker/django/wait_deps.sh ./docker/django/wait_deps.py /app/
WORKDIR /app
USER user
ENTRYPOINT ["/app/wait_deps.sh"]
# command for prod
CMD ["gunicorn", "config.wsgi:application"]


# Python 'run' stage dev
# -----------------------
FROM python-run-prod as python-run-dev

USER root

# get make
RUN apt-get update && apt-get install --no-install-recommends -y \
    make
# copy python dependency wheels from python-build-stage-dev
COPY --from=python-build-stage-dev /usr/src/app/wheels  /wheels/

# use wheels to install python dependencies
RUN pip install --no-cache-dir --no-index --find-links=/wheels/ /wheels/* \
	&& rm -rf /wheels/
COPY --chown=user:user pytest.ini setup.cfg Makefile /app/
COPY --chown=user:user tests /app/

USER user
# command for dev
CMD ["./manage.py", "runserver", "0:8000"]
