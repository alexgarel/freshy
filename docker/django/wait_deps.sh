#! /usr/bin/env bash

# we need this script to be able to run exec after wait_deps
./wait_deps.py
exec "$@"
