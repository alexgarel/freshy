#!/usr/bin/env python3
"""waiting for audit external dependencies
"""
import os
import time


if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'freshy.config.settings')

    WAIT_TIME = 1  # we could get it from environ

    # wait for db to be ready
    from django.db import connection
    from django.db.utils import ProgrammingError
    db_ok = False
    print("waiting for db", flush=True)
    while not db_ok:
        try:
            connection.cursor().execute('SELECT * from django_migrations;')
            db_ok = True
        except ProgrammingError:
            # this is ok, database is up, but table does not exist
            db_ok = True
        except:
            time.sleep(1)
    print("db ready", flush=True)
