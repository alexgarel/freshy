.PHONY: clean clean-test clean-pyc 
.DEFAULT_GOAL := help

clean:clean-pyc clean-test ## remove all

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: ## make style and run flake8
	isort freshy tests
	black freshy tests
	flake8 freshy tests

lint-check: ## verify style
	black --check freshy tests
	isort --ws freshy tests --check-only
	flake8 freshy tests

test: ## run tests quickly with the default Python
	pytest tests
