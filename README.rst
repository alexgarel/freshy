freshy
======

Fresh products on your shelves

:License: MIT

This is an exercice.

This code will propose a very simple API to sync in shop devices.

API Usage
---------

To sync, a device should:

- first push its updates, by a ``POST`` to ``app.domain/api/stock-sync``

  Your updates are given json encoded in the body of the request.
  It is a list of dicts, where each dict contains:
  - shop_id: shop id
  - gtin: product gtin
  - expiry: minimum expiry date for this product in this shop (in iso format)
  - updated: date of acquisition of the data (in iso format)

- then fetch updates, by a ``GET`` to ``app.domain/api/stock-sync``,
  passing in two mandatory parameters:

  - shop_ids: an int that can be repeated, of the shops the device is linked to
  - since: a datetime in iso-format, of last sync
    (put a remote date for last sync)

  eg::

     app.domain/api/stock-sync?shop_ids=1&shop_ids=2&since=2021-07-09 10:00



Setup
-----

First make your docker environment.

You can copy env.sample::

    cp env.sampl .env

Edit and review it, comments should guide you.

There is a sample dev config in .envs/.dev, you can look at it.

Then setup docker-compose::

    docker-compose build && docker-compose pull && docker-compose create

::

    docker-compose run --rm web ./manage.py migrate

And launch::

    docker-compose up

At startup, the app will create an admin user, whose password and username is setup in
``.envs/.your-env/django``.

You must use this user / password to connect to ``/admin`` to populate the app with some shops.



Deploy on Heroku
----------------

**FIXME**


Develop
-------

To run linter::

    docker-compose run --rm --no-deps web make lint

To run tests::

    docker-compose run --rm --no-deps web make test



Credits
-------

(Very) Few parts where taken from Django Cookiecutter project.
