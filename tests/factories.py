import datetime

import factory
import factory.django
from django.utils import timezone

from freshy.stock.models import Shop, StockReading


class ShopFactory(factory.django.DjangoModelFactory):

    title = factory.Sequence(lambda n: f"shop - {n}")

    class Meta:
        model = Shop


class StockReadingFactory(factory.django.DjangoModelFactory):

    gtin = factory.Sequence(lambda n: f"{n:013}")
    shop = factory.SubFactory(ShopFactory)

    expiry = factory.LazyFunction(lambda: timezone.now() - datetime.timedelta(days=-5))
    updated = factory.LazyFunction(timezone.now)

    class Meta:
        model = StockReading


class StockReadingGenerator:
    def days_ago(self, days, hours=0):
        return timezone.now() - datetime.timedelta(days=days, hours=hours)

    def generate_data(self, num_shops, num_records):
        shops = []
        readings = []
        for i in range(num_shops):
            shop = ShopFactory()
            shops.append(shop)
            for j in range(num_records):
                reading = StockReadingFactory(
                    gtin=f"{j:013}",
                    shop=shop,
                    updated=self.days_ago(j),
                    expiry=self.days_ago(j + 5),
                )
                readings.append(reading)
        return shops, readings
