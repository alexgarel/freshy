"""basic tests on models
"""
import datetime

import pytest
from django.urls import reverse

from freshy.stock.models import StockReading


@pytest.mark.django_db(True)
class TestStockSync:

    url = reverse("stock:stock-sync")

    def test_get_empty(self, api_client):
        response = api_client.get(
            self.url, {"since": "2021-01-01 10:00", "shop_ids": 1}
        )
        assert response.status_code == 200
        assert response.data == []

    def test_get_data(self, api_client, stock_gen):
        shops, readings = stock_gen.generate_data(2, 3)
        # get all for shop 1, with remote since
        response = api_client.get(
            self.url, {"since": stock_gen.days_ago(10), "shop_ids": shops[0].id}
        )
        assert response.status_code == 200
        assert len(response.data) == 3
        assert {d["shop_id"] for d in response.data} == {shops[0].id}
        # get none with future since
        response = api_client.get(
            self.url, {"since": stock_gen.days_ago(-1), "shop_ids": shops[0].id}
        )
        assert response.status_code == 200
        assert response.data == []
        # get all for both shop, for last day
        response = api_client.get(
            self.url,
            {"since": stock_gen.days_ago(0, 5), "shop_ids": [shops[0].id, shops[1].id]},
        )
        assert response.status_code == 200
        assert len(response.data) == 2
        assert {d["shop_id"] for d in response.data} == {shops[0].id, shops[1].id}

    def test_get_no_shop_ids(self, api_client):
        response = api_client.get(self.url, {"since": "2021-01-01 10:00"})
        assert response.status_code == 400
        assert "shop_ids" in str(response.data)

    def test_get_bad_shop_ids(self, api_client):
        response = api_client.get(
            self.url, {"since": "2021-01-01 10:00", "shop_ids": "a"}
        )
        assert response.status_code == 400
        assert "shop_ids" in str(response.data)

    def test_get_no_since(self, api_client):
        response = api_client.get(self.url, {"shop_ids": "1"})
        assert response.status_code == 400
        assert "since" in str(response.data)

    def test_get_bad_since(self, api_client):
        response = api_client.get(self.url, {"since": "bad", "shop_ids": "1"})
        assert response.status_code == 400
        assert "since" in str(response.data)

    def test_post_create_all(self, api_client, shop):
        data = [
            {
                "shop_id": shop.id,
                "gtin": "0000000000012",
                "expiry": "2021-01-02 12:00",
                "updated": "2021-01-01 13:12",
            },
            {
                "shop_id": shop.id,
                "gtin": "0000000000013",
                "expiry": "2021-01-03 12:00",
                "updated": "2021-01-01 15:12",
            },
        ]
        response = api_client.post(self.url, data, format="json")
        assert response.status_code == 200
        assert StockReading.objects.count() == 2

    def test_post_update_all(self, api_client, stock_gen):
        shops, readings = stock_gen.generate_data(2, 2)
        reading1 = readings[0]
        reading2 = readings[3]
        data = [
            {
                "shop_id": reading1.shop_id,
                "gtin": reading1.gtin,
                "expiry": reading1.expiry + datetime.timedelta(days=10),
                "updated": reading1.updated + datetime.timedelta(days=1),
            },
            {
                "shop_id": reading2.shop_id,
                "gtin": reading2.gtin,
                "expiry": reading2.expiry + datetime.timedelta(days=10),
                "updated": reading2.updated + datetime.timedelta(days=1),
            },
        ]
        response = api_client.post(self.url, data, format="json")
        assert response.status_code == 200
        # same count as before
        assert StockReading.objects.count() == len(readings)
        # values modified
        new_reading1 = StockReading.objects.get(
            shop_id=reading1.shop_id, gtin=reading1.gtin
        )
        assert new_reading1.expiry == reading1.expiry + datetime.timedelta(days=10)
        assert new_reading1.updated == reading1.updated + datetime.timedelta(days=1)
        new_reading2 = StockReading.objects.get(
            shop_id=reading2.shop_id, gtin=reading2.gtin
        )
        assert new_reading2.expiry == reading2.expiry + datetime.timedelta(days=10)
        assert new_reading2.updated == reading2.updated + datetime.timedelta(days=1)

    def test_post_no_update(self, api_client, stock_gen):
        shops, readings = stock_gen.generate_data(2, 2)
        reading1 = readings[0]
        reading2 = readings[3]
        data = [
            {
                "shop_id": reading1.shop_id,
                "gtin": reading1.gtin,
                "expiry": reading1.expiry + datetime.timedelta(days=10),
                "updated": reading1.updated - datetime.timedelta(days=1),
            },
            {
                "shop_id": reading2.shop_id,
                "gtin": reading2.gtin,
                "expiry": reading2.expiry + datetime.timedelta(days=10),
                "updated": reading2.updated - datetime.timedelta(days=1),
            },
        ]
        response = api_client.post(self.url, data, format="json")
        assert response.status_code == 200
        # same objects
        assert StockReading.objects.count() == len(readings)
        # no changes
        new_reading1 = StockReading.objects.get(
            shop_id=reading1.shop_id, gtin=reading1.gtin
        )
        assert new_reading1.expiry == reading1.expiry
        assert new_reading1.updated == reading1.updated
        new_reading2 = StockReading.objects.get(
            shop_id=reading2.shop_id, gtin=reading2.gtin
        )
        assert new_reading2.expiry == reading2.expiry
        assert new_reading2.updated == reading2.updated

    def test_post_mixed(self, api_client, stock_gen):
        shops, readings = stock_gen.generate_data(2, 2)
        reading1 = readings[0]
        reading2 = readings[3]
        data = [
            # new
            {
                "shop_id": reading1.shop_id,
                "gtin": "1111111111111",
                "expiry": stock_gen.days_ago(2),
                "updated": stock_gen.days_ago(10),
            },
            # real update
            {
                "shop_id": reading1.shop_id,
                "gtin": reading1.gtin,
                "expiry": reading1.expiry + datetime.timedelta(days=10),
                "updated": reading1.updated + datetime.timedelta(days=1),
            },
            # false update
            {
                "shop_id": reading2.shop_id,
                "gtin": reading2.gtin,
                "expiry": reading2.expiry + datetime.timedelta(days=10),
                "updated": reading2.updated - datetime.timedelta(days=1),
            },
        ]
        response = api_client.post(self.url, data, format="json")
        assert response.status_code == 200
        # one more update
        assert StockReading.objects.count() == len(readings) + 1
        # reading1 modified
        new_reading1 = StockReading.objects.get(
            shop_id=reading1.shop_id, gtin=reading1.gtin
        )
        assert new_reading1.expiry == reading1.expiry + datetime.timedelta(days=10)
        assert new_reading1.updated == reading1.updated + datetime.timedelta(days=1)
        # no change on reading2
        new_reading2 = StockReading.objects.get(
            shop_id=reading2.shop_id, gtin=reading2.gtin
        )
        assert new_reading2.expiry == reading2.expiry
        assert new_reading2.updated == reading2.updated
