import pytest
from rest_framework.test import APIClient

from .factories import ShopFactory, StockReadingFactory, StockReadingGenerator


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def shop():
    return ShopFactory()


@pytest.fixture
def stock_reading():
    return StockReadingFactory()


@pytest.fixture
def stock_gen():
    return StockReadingGenerator()


@pytest.fixture
def api_client():
    return APIClient()
