"""basic tests on models
"""
import datetime

import pytest
from django.db.utils import IntegrityError
from django.utils import timezone

from freshy.stock.models import StockReading

from .factories import StockReadingFactory


@pytest.mark.django_db(True)
class TestStockReading:
    def test_stock_reading_unique(self, stock_reading):
        with pytest.raises(IntegrityError):
            StockReadingFactory(gtin=stock_reading.gtin, shop=stock_reading.shop)

    def test_stock_natural_key(self, stock_reading):
        assert stock_reading.natural_key == (stock_reading.shop_id, stock_reading.gtin)

    def test_get_updated(self, stock_gen):
        shops, readings = stock_gen.generate_data(3, 5)
        # updated since 2 days and 1h in shop 1
        result = StockReading.get_updated(
            shop_ids=[shops[0].id], since=stock_gen.days_ago(2, 1)
        )
        assert len(result) == 3
        assert set(r.shop_id for r in result) == {shops[0].id}
        assert set(r.gtin for r in result) == {f"{j:013}" for j in range(3)}
        # updated since 3 days and 1h in shop 1
        result = StockReading.get_updated(
            shop_ids=[shops[0].id, shops[1].id], since=stock_gen.days_ago(3, 1)
        )
        assert len(result) == 8
        assert set(r.shop_id for r in result) == {shops[0].id, shops[1].id}
        assert set(r.gtin for r in result) == {f"{j:013}" for j in range(4)}

        # updated since future !
        result = StockReading.get_updated(
            shop_ids=[shops[0].id, shops[1].id], since=stock_gen.days_ago(-1)
        )
        assert len(result) == 0

        # no shop !
        result = StockReading.get_updated(shop_ids=[], since=stock_gen.days_ago(5))
        assert len(result) == 0

    def test_update_from_device_single(self, stock_gen):
        shops, readings = stock_gen.generate_data(2, 3)

        # push single update on shop 0, gtin 1
        orig_reading = readings[1]
        new_expiry = timezone.now() - datetime.timedelta(days=-20)
        new_updated = timezone.now()
        updated_ids = StockReading.update_from_device(
            [
                StockReadingFactory.build(
                    gtin=orig_reading.gtin,
                    shop_id=orig_reading.shop_id,
                    expiry=new_expiry,
                    updated=new_updated,
                )
            ]
        )
        assert updated_ids == [orig_reading.id]
        reading = StockReading.objects.get(pk=orig_reading.id)
        assert reading.natural_key == orig_reading.natural_key
        assert reading.expiry == new_expiry
        assert reading.updated == new_updated

    def test_update_from_device_older(self, stock_gen):
        shops, readings = stock_gen.generate_data(2, 3)

        # push single update on shop 0, gtin 1, but with older date
        orig_reading = readings[1]
        new_expiry = timezone.now() - datetime.timedelta(days=20)
        new_updated = timezone.now() - datetime.timedelta(days=20)
        updated_ids = StockReading.update_from_device(
            [
                StockReadingFactory.build(
                    gtin=orig_reading.gtin,
                    shop_id=orig_reading.shop_id,
                    expiry=new_expiry,
                    updated=new_updated,
                )
            ]
        )
        assert updated_ids == []
        reading = StockReading.objects.get(pk=orig_reading.id)
        assert reading.natural_key == orig_reading.natural_key
        assert reading.expiry == orig_reading.expiry
        assert reading.updated == orig_reading.updated

    # FIXME: we should rework original code, to be able to test concurrency
