from django.contrib import admin
from django.urls import include, path

from ..stock import urls as stock_urls

urlpatterns = [
    path("api/", include((stock_urls.urlpatterns, "stock"))),
    path("admin/", admin.site.urls),
]
