from django.contrib import admin

from .models import Shop, StockReading


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    pass


@admin.register(StockReading)
class StockReadingAdmin(admin.ModelAdmin):
    pass
