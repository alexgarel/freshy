from rest_framework import serializers

from .models import Shop, StockReading


class StockReadingListSerializer(serializers.ListSerializer):
    """Behaviour for list of StockReading"""

    def update(self):
        """Update book readings and return a updated ids"""
        instances = [StockReading(**data) for data in self.validated_data]
        return StockReading.update_from_device(instances)


class StockReadingSerializer(serializers.ModelSerializer):
    """Serializing stock reading, use with list

    It is used to send state, and to receive new states
    """

    shop_id = serializers.PrimaryKeyRelatedField(
        source="shop", queryset=Shop.objects.all()
    )

    class Meta:
        model = StockReading
        fields = ["gtin", "shop_id", "expiry", "updated"]
        list_serializer_class = StockReadingListSerializer


class SyncFilterSerializer(serializers.Serializer):
    """Serializing filter tc get updates"""

    shop_ids = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=False
    )
    since = serializers.DateTimeField()
