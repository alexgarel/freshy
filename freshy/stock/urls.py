from django.urls import path

from . import views

urlpatterns = [path("stock-sync", views.StockReadingSync.as_view(), name="stock-sync")]
