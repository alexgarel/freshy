import os

from django.apps import AppConfig


class StockConfig(AppConfig):
    name = "freshy.stock"
    label = "stock"

    @classmethod
    def ready(cls):
        from .utils import create_super_user

        try:
            create_super_user(
                os.environ.get("SUPERUSER_NAME"),
                os.environ.get("SUPERUSER_EMAIL"),
                os.environ.get("SUPERUSER_PASSWORD"),
            )
        except Exception:
            pass  # it may happens on first run
