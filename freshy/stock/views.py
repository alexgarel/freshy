from rest_framework import status
from rest_framework.response import Response
from rest_framework.validators import UniqueTogetherValidator
from rest_framework.views import APIView

from .models import StockReading
from .serializers import StockReadingSerializer, SyncFilterSerializer


class StockReadingSync(APIView):
    """API on StockReading dedicated to sync"""

    # TODO: permissions, for now it's public
    # permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        """The remote device send in the list of product

        data il a list of dict, serializable with StockReadingSerializer
        """
        # data to update
        readings = StockReadingSerializer(data=request.data, many=True)
        # tweak, we don't wan readings to check unicity
        readings.child.validators = [
            v
            for v in readings.child.validators
            if not isinstance(v, UniqueTogetherValidator)
        ]
        if not readings.is_valid():
            return Response(readings.errors, status=status.HTTP_400_BAD_REQUEST)
        updated_ids = readings.update()
        return Response(updated_ids)

    def get(self, request):
        """Get updates

        request must contain:
        - a since parameter
        - a shop_ids parameter (list of shop ids)
        """
        sync_filter = SyncFilterSerializer(data=request.GET)
        if not sync_filter.is_valid():
            return Response(sync_filter.errors, status=status.HTTP_400_BAD_REQUEST)
        shop_ids = sync_filter.validated_data["shop_ids"]
        since = sync_filter.validated_data["since"]
        readings = StockReading.get_updated(shop_ids, since=since)
        data = StockReadingSerializer(instance=readings, many=True)
        return Response(data.data)
