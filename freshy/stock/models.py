import functools
import operator
from itertools import groupby

from django.db import models, transaction
from django.db.models import Q

from .utils import batch_list


class Shop(models.Model):

    title = models.CharField("Title", max_length=256, blank=True)

    def __str__(self):
        return f"Shop: {self.title}"


class StockReading(models.Model):

    UPDATE_BATCH_SIZE = 100

    shop = models.ForeignKey("Shop", on_delete=models.CASCADE)
    # assuming gtin-13
    gtin = models.CharField("GTIN", max_length=13, db_index=True, blank=True)
    expiry = models.DateTimeField("expiry")
    updated = models.DateTimeField("updated")

    class Meta:
        # natural key
        unique_together = [("shop", "gtin")]

    @property
    def natural_key(self):
        """Natural key to select readings"""
        return (self.shop_id, self.gtin)

    @classmethod
    def get_updated(self, shop_ids, since):
        """Fetch updates for a device

        :param shop_ids: known device shop
        :param datetime.datetime since: date of last sync
        :param list exclude_ids: list of StockReading ids to exclude
        """
        qs = self.objects.filter(shop__in=shop_ids, updated__gte=since)
        return qs

    @classmethod
    def _create_from_device(cls, shop_id, stock_readings):
        """Create stock readings that are not yet in database

        :return tuple: set gtin of created readings,  ids of created readings
        """
        shop_readings = cls.objects.filter(shop_id=shop_id)
        gtins = [reading.gtin for reading in stock_readings]
        with transaction.atomic():
            # lock shop for insertion
            Shop.objects.filter(id=shop_id).select_for_update()
            new_gtins = set(gtins) - set(
                shop_readings.filter(gtin__in=gtins).values_list("gtin", flat=True)
            )
            # create
            new_readings = [
                reading for reading in stock_readings if reading.gtin in new_gtins
            ]
            created = cls.objects.bulk_create(new_readings) if new_readings else []
        return new_gtins, [reading.id for reading in created]

    @classmethod
    def _update_from_device(cls, shop_id, stock_readings):
        """Update stock readings"""
        if not stock_readings:
            # quick exit
            return []
        shop_readings = cls.objects.filter(shop_id=shop_id)
        exprs = [
            Q(gtin=reading.gtin) & Q(updated__lt=reading.updated)
            for reading in stock_readings
        ]
        expr = functools.reduce(operator.or_, exprs)
        by_key = {reading.natural_key: reading for reading in stock_readings}
        with transaction.atomic():
            #  select and lock
            to_update = (
                shop_readings.filter(expr)
                .only("id", "gtin", "shop_id")
                .select_for_update()
            )
            #  update
            for reading in to_update:
                new_data = by_key[reading.natural_key]
                reading.expiry = new_data.expiry
                reading.updated = new_data.updated
            cls.objects.bulk_update(to_update, fields=["expiry", "updated"])
        return [reading.id for reading in to_update]

    @classmethod
    def _update_batch_from_device(cls, shop_id, stock_readings):
        """make an update for a batch of data

        :see:`update_from_device`
        """
        # first create new rows (best for competition)
        new_gtins, created_ids = cls._create_from_device(shop_id, stock_readings)

        # update
        updatable = [
            reading for reading in stock_readings if reading.gtin not in new_gtins
        ]
        updated_ids = cls._update_from_device(shop_id, updatable)
        return created_ids + updated_ids

    @classmethod
    def update_from_device(cls, stock_readings):
        """Update a shop with stock_readings

        :param list stock_readings: list of StockReading

        :return list: updated ids
        """
        # split by shop (there should be few)
        stock_readings = sorted(stock_readings, key=lambda d: d.shop_id)
        by_shop = {
            shop_id: list(shop_stock_readings)
            for shop_id, shop_stock_readings in groupby(
                stock_readings, key=lambda d: d.shop_id
            )
        }
        updated_ids = []
        for shop_id, shop_readings in by_shop.items():
            # batch to avoid big updates, and therefor long locks
            for batch in batch_list(shop_readings, cls.UPDATE_BATCH_SIZE):
                batch_updated_ids = cls._update_batch_from_device(
                    shop_id, shop_readings
                )
                updated_ids.extend(batch_updated_ids)
        return updated_ids
