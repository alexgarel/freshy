"""Some utility classes
"""
from django.contrib.auth import get_user_model


def batch_list(lst, size):
    """Batch a list"""
    for i in range(0, len(lst), size):
        start = i * size
        yield lst[start : start + size]  # noqa:E203


def create_super_user(name, email, pwd):
    if not (name and email and pwd):
        return
    user_model = get_user_model()
    if not user_model.objects.filter(username=name).first():
        user_model.objects.create_superuser(name, email, pwd)
